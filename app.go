package main


import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"strconv"
	"strings"
	"fmt"
)

const tokenlink string = "./config/token.txt"

func main() {
	token, err := readBotToken(tokenlink)
	if err != nil {
		log.Panicf("Token error: ", err)
	}
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}
	var adminchat int64 = -1001977317838
	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message != nil {
			m := update.Message
			if m.From == nil {
				continue
			}
			if m.Text == "/start" && m.Chat.Type == "private" {
				text := "привет!\nэто бот канала «[Рефлексии и Откровения](https://t.me/reflectionsandrevelations)», и всё, что ты сюда напишешь — отправится Маше 📖\n\nона уже ждёт твои вопросы и истории!"
				msg := tgbotapi.NewMessage(m.Chat.ID, text)
				msg.ParseMode = "Markdown"
				msg.DisableWebPagePreview = true
				bot.Send(msg)
			} else if m.Chat.Type == "private" {
				fullName := m.From.FirstName + " " + m.From.LastName
				if m.Text != "" {
					if m.ForwardFrom != nil {
						text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName + "\n" + "Forwarded from: " + "<a href='tg://user?id=" + strconv.FormatInt(m.ForwardFrom.ID, 10) + "'>" + m.ForwardFrom.UserName + "</a>"
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						msg.DisableWebPagePreview = true
						bot.Send(msg)
						text = m.Text
						msg = tgbotapi.NewMessage(adminchat, text)
						bot.Send(msg)
					} else {
						text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						msg.DisableWebPagePreview = true
						bot.Send(msg)
						text = m.Text
						msg = tgbotapi.NewMessage(adminchat, text)
						bot.Send(msg)
					}
				} else if m.Photo != nil {
					text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName
					msg := tgbotapi.NewMessage(adminchat, text)
					msg.ParseMode = "HTML"
					msg.DisableWebPagePreview = true
					bot.Send(msg)
					if m.ForwardFrom != nil {
						text = strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName + "\n" + "Forwarded from: " + "<a href='tg://user?id=" + strconv.FormatInt(m.ForwardFrom.ID, 10) + "'>" + m.ForwardFrom.UserName + "</a>"
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						msg.DisableWebPagePreview = true
						bot.Send(msg)
					}
					text = m.Caption
					existingFileID := m.Photo[len(m.Photo)-1].FileID
					photoMsg := tgbotapi.NewPhoto(adminchat, tgbotapi.FileID(existingFileID))
					photoMsg.Caption = text
					bot.Send(photoMsg)
				} else if m.Audio != nil {
					text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName
					msg := tgbotapi.NewMessage(adminchat, text)
					msg.ParseMode = "HTML"
					msg.DisableWebPagePreview = true
					bot.Send(msg)
					if m.ForwardFrom != nil {
						text = strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName + "\n" + "Forwarded from: " + "<a href='tg://user?id=" + strconv.FormatInt(m.ForwardFrom.ID, 10) + "'>" + m.ForwardFrom.UserName + "</a>"
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						msg.DisableWebPagePreview = true
						bot.Send(msg)
					}
					text = m.Caption
					existingFileID := m.Audio.FileID
					audioMsg := tgbotapi.NewAudio(adminchat, tgbotapi.FileID(existingFileID))
					audioMsg.Caption = text
					audioMsg.ParseMode = "HTML"
					bot.Send(audioMsg)
				} else if m.Document != nil {
					text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName
					msg := tgbotapi.NewMessage(adminchat, text)
					msg.ParseMode = "HTML"
					msg.DisableWebPagePreview = true
					bot.Send(msg)
					if m.ForwardFrom != nil {
						text = strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName + "\n" + "Forwarded from: " + "<a href='tg://user?id=" + strconv.FormatInt(m.ForwardFrom.ID, 10) + "'>" + m.ForwardFrom.UserName + "</a>"
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						msg.DisableWebPagePreview = true
						bot.Send(msg)
					}
					text = m.Caption
					existingFileID := m.Document.FileID
					documentMsg := tgbotapi.NewDocument(adminchat, tgbotapi.FileID(existingFileID))
					documentMsg.Caption = text
					documentMsg.ParseMode = "HTML"
					bot.Send(documentMsg)
				} else if m.Video != nil {
					text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName
					msg := tgbotapi.NewMessage(adminchat, text)
					msg.ParseMode = "HTML"
					msg.DisableWebPagePreview = true
					bot.Send(msg)
					if m.ForwardFrom != nil {
						text = strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName + "\n" + "Forwarded from: " + "<a href='tg://user?id=" + strconv.FormatInt(m.ForwardFrom.ID, 10) + "'>" + m.ForwardFrom.UserName + "</a>"
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						msg.DisableWebPagePreview = true
						bot.Send(msg)
					}
					text = m.Caption
					existingFileID := m.Video.FileID
					videoMsg := tgbotapi.NewVideo(adminchat, tgbotapi.FileID(existingFileID))
					videoMsg.Caption = text
					videoMsg.ParseMode = "HTML"
					bot.Send(videoMsg)
				} else if m.Voice != nil {
					text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName
					msg := tgbotapi.NewMessage(adminchat, text)
					msg.ParseMode = "HTML"
					msg.DisableWebPagePreview = true
					bot.Send(msg)
					if m.ForwardFrom != nil {
						text = strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName + "\n" + "Forwarded from: " + "<a href='tg://user?id=" + strconv.FormatInt(m.ForwardFrom.ID, 10) + "'>" + m.ForwardFrom.UserName + "</a>"
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						msg.DisableWebPagePreview = true
						bot.Send(msg)
					}
					existingFileID := m.Voice.FileID
					voiceMsg := tgbotapi.NewVoice(adminchat, tgbotapi.FileID(existingFileID))
					bot.Send(voiceMsg)
				} else if m.VideoNote != nil {
					text := strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName
					msg := tgbotapi.NewMessage(adminchat, text)
					msg.ParseMode = "HTML"
					bot.Send(msg)
					if m.ForwardFrom != nil {
						text = strconv.FormatInt(m.Chat.ID, 10) + " - " + "<a href='tg://user?id=" + strconv.FormatInt(m.Chat.ID, 10) + "'>" + m.From.UserName + "</a>" + " - " + fullName + "\n" + "Forwarded from: " + "<a href='tg://user?id=" + strconv.FormatInt(m.ForwardFrom.ID, 10) + "'>" + m.ForwardFrom.UserName + "</a>"
						msg := tgbotapi.NewMessage(adminchat, text)
						msg.ParseMode = "HTML"
						bot.Send(msg)
					}
					msg = tgbotapi.NewMessage(adminchat, text)
					existingFileID := m.VideoNote.FileID
					length := m.VideoNote.Length
					videoNoteMsg := tgbotapi.NewVideoNote(adminchat, length, tgbotapi.FileID(existingFileID))
					bot.Send(videoNoteMsg)
				} 
			} else if m.Chat.ID == adminchat && m.ReplyToMessage != nil {
				originalmessage := m.ReplyToMessage
				if originalmessage.From.ID != 5811891942{
					continue
				} else{

					messagetext := originalmessage.Text
					if originalmessage.Caption != "" {
						messagetext = originalmessage.Text
					}
					words := strings.Fields(messagetext)

					// Check if there's at least one word
					if len(words) > 0 {
						firstWord := words[0]

						// Convert to int64
						replychat, err := strconv.ParseInt(firstWord, 10, 64)
						if err != nil {
							// Handle error
							fmt.Println("Error:", err)
							continue
						}
						if m.Text != "" {
							msg := tgbotapi.NewMessage(replychat, m.Text)
							bot.Send(msg)
						} else if m.Photo != nil {
							existingFileID := m.Photo[len(m.Photo)-1].FileID
							photoMsg := tgbotapi.NewPhoto(replychat, tgbotapi.FileID(existingFileID))
							photoMsg.Caption = m.Caption
							bot.Send(photoMsg)
						} else if m.Audio != nil {
							existingFileID := m.Audio.FileID
							audioMsg := tgbotapi.NewAudio(replychat, tgbotapi.FileID(existingFileID))
							audioMsg.Caption = m.Caption
							bot.Send(audioMsg)
						} else if m.Document != nil {
							existingFileID := m.Document.FileID
							documentMsg := tgbotapi.NewDocument(replychat, tgbotapi.FileID(existingFileID))
							documentMsg.Caption = m.Caption
							bot.Send(documentMsg)
						} else if m.Video != nil {
							existingFileID := m.Video.FileID
							videoMsg := tgbotapi.NewVideo(replychat, tgbotapi.FileID(existingFileID))
							videoMsg.Caption = m.Caption
							bot.Send(videoMsg)
						} else if m.Voice != nil {
							existingFileID := m.Voice.FileID
							voiceMsg := tgbotapi.NewVoice(replychat, tgbotapi.FileID(existingFileID))
							bot.Send(voiceMsg)
						} else if m.VideoNote != nil {
							existingFileID := m.VideoNote.FileID
							length := m.VideoNote.Length
							videoNoteMsg := tgbotapi.NewVideoNote(replychat, length, tgbotapi.FileID(existingFileID))
							bot.Send(videoNoteMsg)
						}
					}
				}
			}
		}
	}
}
